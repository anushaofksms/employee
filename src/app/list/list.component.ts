import { Component, OnInit } from '@angular/core';
import { ListService } from './../list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  
  list:any;
  hide:boolean;
  employeeId:any;
  empSalary:any;
  addForm:boolean;
  listForm:boolean;
  editForm:boolean;
  userId:any;
  empName:any;
  index:any;

  fetchData =
  {
    "id":"",
    "name":"",
    "salary":"",
    "empId":""
  }

  constructor(private listService:ListService) { 
     this.list=[];
     this.hide = false
     this.addForm = false
     this.listForm = true
     this.editForm = false
  }

  ngOnInit() {
    
    this.getList();
  }
  
  getList()
  {
    try {
      this.listService.getList()
        .subscribe(response => {
          this.list=response
          // console.log(this.list);
        }, error => {
          console.log('==Get error Details==', error)
        })
    } catch (ex) {
      console.log(ex)
    }

  }

  userData(data,i)
  {
     this.hide = true
     console.log(data);
     this.employeeId = data.empId;
     this.empSalary = data.salary;
     this.userId = data.id;
     this.empName = data.name;
     this.index = i;
  }

  addUser(user)
  {
    console.log(user.value);
    this.list.push(user.value);
    this.listForm = true
    this.addForm = false
   
  }

  enableAdd()
  {
    this.addForm = true
    this.listForm = false
  }

  editClick()
  {
     this.editForm = true;
     this.addForm = false;
     this.listForm = false;
     this.fetchData.id = this.userId;
     this.fetchData.name = this.empName;
     this.fetchData.salary = this.empSalary;
     this.fetchData.empId = this.employeeId;
  }

  editUser(editData)
  {
     for(var i=0;i<this.list.length;i++)
     {
       if(this.list[i].id == editData.id)
       {
        this.list[i].name=editData.name,
        this.list[i].salary=editData.salary,
        this.list[i].empId=editData.empId
       }
     }
     this.editForm = false;
     this.addForm = false;
     this.listForm = true;
  }

  deleteGetId()
  {
    this.list.splice(this.index,1);
  }
}
